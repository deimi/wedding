/*!
 * Start Bootstrap - Grayscale Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// remove the focused state after click,
// otherwise bootstrap will still highlight the link
$("a").mouseup(function(){
    $(this).blur();
})

// Google Maps Scripts
// When the window has finished loading create our google map below
google.maps.event.addDomListener(window, 'load', init);

function init() {
    var myLatLng = new google.maps.LatLng(47.195694, 11.300483);

    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: 15,

        // The latitude and longitude to center the map (always required)
        // center: new google.maps.LatLng(40.6700, -73.9400), // New York
        center: myLatLng, 

        // Disables the default Google Maps UI components
        disableDefaultUI: false,
        scrollwheel: false,
        draggable: true,
        mapTypeControl: false,
        streetViewControl: false,

        // How you would like to style the map. 
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{
            // "featureType": "water",
            // "elementType": "geometry",
            // "stylers": [{
            //     "color": "#303030"
            // }, {
            //     "lightness": 17
            // }]
        // }, {
        //     "featureType": "landscape",
        //     "elementType": "geometry",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 50
        //     }]
        // }, {
        //     "featureType": "road.highway",
        //     "elementType": "geometry.fill",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 17
        //     }]
        // }, {
        //     "featureType": "road.highway",
        //     "elementType": "geometry.stroke",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 29
        //     }, {
        //         "weight": 0.2
        //     }]
        // }, {
        //     "featureType": "road.arterial",
        //     "elementType": "geometry",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 18
        //     }]
        // }, {
        //     "featureType": "road.local",
        //     "elementType": "geometry",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 16
        //     }]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [{
                "color": "#303030"
            }, {
                "lightness": 21
            }]
        // }, {
        //     "elementType": "labels.text.stroke",
        //     "stylers": [{
        //     //     "visibility": "on"
        //     // }, {
        //         "color": "#303030"
        //     }, {
        //         "lightness": 16
        //     }]
        }, {
            "elementType": "labels.text.fill",
            "stylers": [{
                "saturation": 36
            }, {
                "color": "#303030"
            }, {
                "lightness": 40
            }]
        }, {
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        // }, {
        //     "featureType": "transit",
        //     "elementType": "geometry",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 19
        //     }]
        // }, {
        //     "featureType": "administrative",
        //     "elementType": "geometry.fill",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 20
        //     }]
        // }, {
        //     "featureType": "administrative",
        //     "elementType": "geometry.stroke",
        //     "stylers": [{
        //         "color": "#303030"
        //     }, {
        //         "lightness": 17
        //     }, {
        //         "weight": 1.2
        //     }]
        }]
    };

    // Get the HTML DOM element that will contain your map 
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');

    // Create the Google Map using out element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    // Custom Map Marker Icon - Customize the map-marker.png file to customize your icon
    var image = 'img/map-marker.png';
    
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hotel Lizum 1600',
        animation: google.maps.Animation.DROP,
        icon: image,
    });

    var infoWindow = new SnazzyInfoWindow({
        marker: beachMarker,
        placement: 'left',
        offset: {
            left: '-15px'
        },
        content: '<div><strong>Hotel Lizum 1600</strong></div>' +
                 '<div>Axamer Lizum 19</div>' +
                 '<div>6094 Axamer Lizum</div>' +
                 '<a href="https://www.lizum1600.at/" target="_blank" >lizum1600.at</a>',
        showCloseButton: true,
        closeOnMapClick: false,
        padding: '20px',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        border: false,
        borderRadius: '0px',
        shadow: false,
        fontColor: '#fff',
        fontSize: '15px'
    });
    infoWindow.open();

    var direction = new google.maps.Polyline({
        map: map,
        path: [
            {lat: 47.196654, lng: 11.302605},
            {lat: 47.195774, lng: 11.302602},
            {lat: 47.195672, lng: 11.303590},
            {lat: 47.194366, lng: 11.303885},
            {lat: 47.194319, lng: 11.302329},
            {lat: 47.195347, lng: 11.300585}
        ],
        icons: [
            {
              icon: {
                        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW, 
                        scale: 2,
                        strokeColor: "#ef8fff"
                    },
              offset: "100%",
              repeat: "20px"
            }
        ],
        strokeColor: "#ef8fff"
    });
}
